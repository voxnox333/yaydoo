/*
    ¿Cual es sueldo promedio por departamento?
*/


SELECT d.dept_no,d.dept_name , AVG( s.salary ) as avg_salary
FROM departments d 
INNER JOIN current_dept_emp cde ON d.dept_no=cde.dept_no 
INNER JOIN last_salary_emp s ON s.emp_no=cde.emp_no 
GROUP BY d.dept_no; 


/*
    ¿Quien es el  administrador con más personas a su cargo y cual es su salario?
*/  

SELECT e.emp_no, e.first_name, e.last_name , cdm.dept_no , d.dept_name , s.salary , cemps.count_emp
FROM current_dept_manager  cdm 
INNER JOIN employees e ON cdm.emp_no = e.emp_no 
INNER JOIN departments d ON d.dept_no = cdm.dept_no 
INNER JOIN last_salary_emp s ON s.emp_no = cdm.emp_no 
INNER JOIN ( 
    SELECT cde.dept_no , COUNT(*) as count_emp
    FROM current_dept_emp cde 
    GROUP BY cde.dept_no ) as cemps ON cemps.dept_no = cdm.dept_no
ORDER BY cemps.count_emp DESC
LIMIT 1; 

/*
    ¿Cual es la edad promedio de contratación por departamento?
*/

SELECT d.dept_no,d.dept_name , AVG( TIMESTAMPDIFF(YEAR,e.birth_date,NOW()) ) as age
FROM departments d 
INNER JOIN current_dept_emp cde ON d.dept_no=cde.dept_no 
INNER JOIN employees e ON cde.emp_no = e.emp_no 
GROUP BY d.dept_no; 


/*
    ¿Cuales son las 10 personas que han tenido un mayor incremento de salario?
*/

SELECT e.emp_no , e.first_name , e.last_name , (ls.salary-fs.salary) as diffsalary
FROM employees e
INNER JOIN first_salary_emp fs ON fs.emp_no=e.emp_no
INNER JOIN last_salary_emp ls ON ls.emp_no=e.emp_no
ORDER BY diffsalary DESC
LIMIT 10;


/*
    ¿Cual es el departamento donde hay más rotación de personal?
*/    

SELECT d.dept_no,d.dept_name , de.count_rot 
FROM departments d 
INNER JOIN ( 
    SELECT dept_no,COUNT(*) as count_rot 
    FROM dept_emp 
    WHERE to_date!='9999-01-01'
    GROUP BY dept_no
)  de ON d.dept_no=de.dept_no 
ORDER BY  de.count_rot DESC
LIMIT 1;
/* VIEWS*/

CREATE OR REPLACE VIEW dept_emp_latest_date AS
    SELECT emp_no, MAX(from_date) AS from_date, MAX(to_date) AS to_date
    FROM dept_emp
    GROUP BY emp_no;

CREATE OR REPLACE VIEW salary_emp_latest_date AS
    SELECT emp_no,MAX(from_date) AS from_date, MAX(to_date) AS to_date
    FROM salaries  
    GROUP BY emp_no;

CREATE OR REPLACE VIEW last_salary_emp AS
    SELECT sl.emp_no, s.salary , sl.from_date, sl.to_date
    FROM salaries s
        INNER JOIN salary_emp_latest_date sl
        ON s.emp_no=sl.emp_no AND s.from_date=sl.from_date AND s.to_date = sl.to_date;

CREATE OR REPLACE VIEW last_dept_emp AS
    SELECT l.emp_no, dept_no, l.from_date, l.to_date
    FROM dept_emp d
        INNER JOIN dept_emp_latest_date l
        ON d.emp_no=l.emp_no AND d.from_date=l.from_date AND l.to_date = d.to_date;


CREATE OR REPLACE VIEW current_dept_emp AS
    SELECT emp_no, dept_no, from_date 
    FROM last_dept_emp 
    WHERE to_date='9999-01-01';
    
CREATE OR REPLACE VIEW dept_manager_latest_date AS
    SELECT dept_no, MAX(from_date) AS from_date, MAX(to_date) AS to_date
    FROM dept_manager 
    GROUP BY dept_no;

CREATE OR REPLACE VIEW current_dept_manager AS
    SELECT dm.emp_no, dl.dept_no , dl.from_date, dl.to_date
    FROM dept_manager dm
        INNER JOIN dept_manager_latest_date dl
        ON dm.dept_no=dl.dept_no AND dm.from_date=dl.from_date AND dm.to_date = dl.to_date;


CREATE OR REPLACE VIEW salary_emp_first_date AS
    SELECT emp_no,MIN(from_date) AS from_date, MIN(to_date) AS to_date
    FROM salaries  
    GROUP BY emp_no;

CREATE OR REPLACE VIEW first_salary_emp AS
    SELECT sf.emp_no, s.salary , sf.from_date, sf.to_date
    FROM salaries s
        INNER JOIN salary_emp_first_date sf
        ON s.emp_no=sf.emp_no AND s.from_date=sf.from_date AND s.to_date = sf.to_date;
